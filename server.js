require('dotenv').config();
const express = require('express');
const body_parser = require('body-parser');
const app = express();
const port = process.env.PORT || 3000;
const URL_BASE = '/techu/v1/';

const usersFile = require('./user.json');

app.listen(port, function(){
  console.log('NodeJS port: ', port);
});

app.use(body_parser.json());

app.get('/', function(req, req){
  req.send("Hola API xy");
});

app.get(URL_BASE + 'users', function(req, res){
  res.send(usersFile);
});

app.get(URL_BASE + 'users/:id', function(req, res){
  console.log(req.params);
  let pos = req.params.id - 1;
  res.send(usersFile[pos]);
});

app.post(URL_BASE + 'users/:id', function(req, res){
  console.log(req.params);
  let pos = req.params.id - 1;
  res.send(usersFile[pos]);
});

app.put(URL_BASE + 'users/:id', function (req, res) {
  console.log("PUT /techu/v1/users/:id");
  let idBuscar = req.params.id;
  let updateUser = req.body;
  for (i = 0; i < usersFile.length; i++) {
    console.log(usersFile[i].id);
    if (usersFile[i].id == idBuscar) {
      usersFile[i] = updateUser;
      res.send({ "msg": "Usuario actualizado correctamente.", updateUser });
    }
  }
  res.send({ "msg": "Usuario no encontrado.", updateUser });
});

app.delete(URL_BASE + 'users/:id', function(req, res){
  console.log(req.params);
  let pos = req.params.id - 1;
  res.send(usersFile[pos]);
});

app.post(URL_BASE + 'login',
  function(request, response) {
    console.log("POST /apicol/v2/login");
    console.log(request.body.email);
    console.log(request.body.password);

    var user = request.body.email;
    var pass = request.body.password;

    for(us of usersFile) {
      if(us.email == user){
        if(us.password == pass){
          us.logged = true;
          writeUserDataToFile(usersFile);
          console.log("Login correcto!");
          response.status(201).send({"msg" : "Login correcto.", "idUsuario" : us.id, "logged" : "true"});
          return;
        }
        break;
      }
    }

    console.log("Sin Coincidencia.");
    response.status(404).send({"msg" : "Login incorrecto."});
});

app.post(URL_BASE + 'logout', function (request, response) {
  console.log("POST /apicol/v2/logout");
  var userId = request.body.id;
  for (us of usersFile) {
    if (us.id == userId) {
      if (us.logged) {
        delete us.logged; // borramos propiedad 'logged'
        writeUserDataToFile(usersFile);
        console.log("Logout correcto!");
        response.status(201).send({ "msg": "Logout correcto.", "idUsuario": us.id });
        return;
      }
      break;
    }
  }
  console.log("Sin Coincidencia.");
  response.status(404).send({ "msg": "Logout incorrecto." });
});

app.get(URL_BASE + 'total_users', function (request, response) {
  console.log("GET users/total_users");
  response.status(200).send({ "num_usuarios": usersFile.length });
});

function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./user.json", jsonUserData, "utf8",
    function (err) { //función manejadora para gestionar errores de escritura
      if (err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    });
}
